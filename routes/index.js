require('../config/config');
const express = require('express');

const app = express();

app.use('/projectx/v1/users', require('./users'));

module.exports = app;