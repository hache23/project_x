const express = require('express');
const controller = require('../controller/userController');

const router = express.Router();

router.post('/', controller.saveUser);
router.get('/', controller.getUser);

module.exports = router;