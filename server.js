const config = require('config');
const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes/index');
const cors = require('cors');


//Inicializa express
var app = express();
app.use(cors());
app.use(routes);

const rawBodySaver = function (req, res, buf, encoding) {
    if (buf && buf.length) {
        req.rawBody = buf.toString(encoding || 'utf8');
    }
}

app.use(bodyParser.raw({
    verify: rawBodySaver, type: function () {
        return true
    }
}));

const port = config.get('app_port')

if (!port) {
    console.log('FATAL ERROR: variable PORT is not defined.');
    process.exit(1);
}



app.listen(port, () => {
    console.log('*** Server is up and running on port number ' + port + '\n');
});