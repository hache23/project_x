const log4js = require('log4js');
const logger = log4js.getLogger();

logger.level = 'debug';

async function saveUser(req, res) {
    logger.info('[saveUser] INIT');
    try {
        res.status(200)
            .json({message: 'It is Ok'});
    } catch (e) {
        logger.error(`[saveUser] ${e}`);
        res.status(400).json({error: e.message});
    }
    logger.info('[saveUser] FINISH');
}

async function getUser(req, res) {

}

module.exports = {
    saveUser,
    getUser
}